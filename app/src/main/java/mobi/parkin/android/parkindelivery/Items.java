package mobi.parkin.android.parkindelivery;

public class Items {
    public int icon;
    public String itemName;
    public String itemPrice;
    public String itemWeight;

    public Items(){
        super();
    }


    public Items(int icon, String itemName, String itemPrice, String itemWeight) {
        super();
        this.icon = icon;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemWeight = itemWeight;

    }
}
