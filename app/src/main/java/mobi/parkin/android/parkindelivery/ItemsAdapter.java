package mobi.parkin.android.parkindelivery;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemsAdapter extends ArrayAdapter<Items> {

    Context context;
    int layoutResourceId;
    Items data[] = null;

    public ItemsAdapter(Context context, int layoutResourceId, Items[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ItemsAdapter.ItemsHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ItemsAdapter.ItemsHolder();
            holder.imgIcons = (ImageView)row.findViewById(R.id.imgIcons);
            holder.itemName = (TextView)row.findViewById(R.id.itemName);
            holder.itemPrice = (TextView)row.findViewById(R.id.itemPrice);
            holder.itemWeight = (TextView)row.findViewById(R.id.itemWeight);
            row.setTag(holder);
        }
        else
        {
            holder = (ItemsAdapter.ItemsHolder)row.getTag();
        }

        Items items = data[position];
        holder.imgIcons.setImageResource(items.icon);
        holder.itemName.setText(items.itemName);
        holder.itemPrice.setText(items.itemPrice);
        holder.itemWeight.setText(items.itemWeight);


        return row;
    }

    static class ItemsHolder
    {
        ImageView imgIcons;
        TextView itemName;
        TextView itemPrice;
        TextView itemWeight;

    }
}

