package mobi.parkin.android.parkindelivery;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomerLogin extends AppCompatActivity {
    private SignInButton mGoogleBtn;

    private ImageView Googleloginbtn;

    public static final int RC_SIGN_IN = 1;
    // public static final int RC_SIGN_OUT = 2;
    public static GoogleApiClient mGoogleSignInClient;



    private DatabaseReference vehicleDBCar;
    private DatabaseReference vehicleDBBike;
    private DatabaseReference historyDB;
    private String userID;
    public FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;

    public SharedPrefManager sharedPrefManager;
    SharedPreferences hPreferences;

    private FirebaseAnalytics mFirebaseAnalytics;


    private final Context mContext = this;

    private static final String TAG = "User_Login_Activity";

    private boolean newUser;

    ArrayList<String> HistoryList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (firebaseAuth.getCurrentUser() != null) {

                    getUserInfoGoogle();
                    finish();
                    return;
                }
            }
        };

        mGoogleBtn = (SignInButton) findViewById(R.id.googleBtn);
        Googleloginbtn = (ImageView)findViewById(R.id.googleloginbtn);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        Toast.makeText(CustomerLogin.this, "You Got an Error", Toast.LENGTH_LONG).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mGoogleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });

        Googleloginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signIn();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();



    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListener);
    }

    private void signIn() {


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "2");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "GoogleLoginSuccess");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
                mFirebaseAnalytics.logEvent("GoogleLoginSuccess", bundle);


                //     getUserInfoGoogle();
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "GoogleLoginFailed");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
                mFirebaseAnalytics.logEvent("LoginFailed", bundle);

                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "1");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "GoogleLoginAttempt");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);


        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            mAuth.addAuthStateListener(mAuthListener);

                            sharedPrefManager = new SharedPrefManager(mContext);
                            sharedPrefManager.setFirstTimeLaunch(false);

                            Log.d(TAG, "signInWithCredential:success");
                            newUser = task.getResult().getAdditionalUserInfo().isNewUser();
                            if (newUser) {
                                //     getUserInfoGoogle();

                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(CustomerLogin.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                            //    Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //    updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void getUserInfoGoogle() {
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();

            Log.d("person name:", personName);
            Log.d("person Givenname:", personGivenName);
            Log.d("person Familyname:", personFamilyName);
            Log.d("person email:", personEmail);
            Log.d("person id:", personId);
            Log.d("person pic:", String.valueOf(personPhoto));

            mAuth = FirebaseAuth.getInstance();
            String user_id = mAuth.getCurrentUser().getUid();
            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);

            current_user_db.child("UserInformation").child("userEmail").setValue(personEmail);
            current_user_db.child("UserInformation").child("userName").setValue(personGivenName);
            //  current_user_db.setValue(true);


            sharedPrefManager = new SharedPrefManager(mContext);

            sharedPrefManager.saveEmail(this, personEmail);
            sharedPrefManager.saveName(this, personGivenName);
            sharedPrefManager.savePhoto(this, String.valueOf(personPhoto));

//            String x= sharedPrefManager.getState();
//            sharedPrefManager.saveState(this, x);


            getVehicleInfo();


        }
    }

    private void getVehicleInfo() {
        ArrayList<ClipData.Item> yourStringArray;
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        vehicleDBCar = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles").child("Car");
        vehicleDBBike = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles").child("Bike");

        vehicleDBCar.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long value = dataSnapshot.getChildrenCount();
                Log.d(TAG, "no of children: " + value);

                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>() {
                };

                List<String> taskDesList = dataSnapshot.getValue(genericTypeIndicator);


                hPreferences = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
                // Set<String> car = hPreferences.getStringSet("car",null);
                Set<String> car = new HashSet<String>();
                SharedPreferences.Editor editor = hPreferences.edit();
                //  Set<String> car = new HashSet<String>();

                if (taskDesList != null) {
                    car.addAll(taskDesList);
                }

                editor.putStringSet("car", car);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        vehicleDBBike.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long values = dataSnapshot.getChildrenCount();
                Log.d(TAG, "no of children: " + values);

                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>() {
                };


                List<String> taskDesLists = dataSnapshot.getValue(genericTypeIndicator);



                hPreferences = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
                // Set<String> car = hPreferences.getStringSet("car",null);
                Set<String> bike = new HashSet<String>();
                SharedPreferences.Editor editor = hPreferences.edit();
                //  Set<String> car = new HashSet<String>();

                if (taskDesLists != null) {
                    bike.addAll(taskDesLists);
                }
                editor.putStringSet("bike", bike);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        historyDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("UserParkingHistory");
        historyDB.orderByChild("UserParkingHistory").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                HistoryList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                   // String x = snapshot.getKey();
                    String entry = String.valueOf(snapshot.getValue());


                    HistoryList.add(entry);

                    ArrayList<String> addHistoryList = new ArrayList<String>();



                  //  Log.d("x", x);
                    Log.d("Histry", entry);
                }

                hPreferences = getSharedPreferences("SaveHistory",Context.MODE_PRIVATE);
                //Set<String> transaction = hPreferences.getStringSet("transaction",null);
                Set<String> transaction = new HashSet<String>();
                SharedPreferences.Editor editor= hPreferences.edit();

                transaction.addAll(HistoryList);
//                    if (transaction!=null) {
//                        for (String x : transaction) {
//                            addHistoryList.add(x);
//                        }
//                    }
                editor.putStringSet("transaction",transaction);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



    public static void googleLogOut() {



        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleSignInClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        //updateUI(null);
                    }
                });
    }


       @Override
    public void onBackPressed() {
        // super.onBackPressed(); commented this line in order to disable back press
        //Write your code here
     //   Toast.makeText(getApplicationContext(), "Back press disabled!", Toast.LENGTH_SHORT).show();
    }


}
