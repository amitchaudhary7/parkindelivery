package mobi.parkin.android.parkindelivery;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;


public class Find extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener {

    public SharedPrefManager sharedPrefManager;
   // SharedPrefManager sharedPrefManager;
    private GoogleMap mMap;
    SharedPreferences hPreferences;

    ArrayList<String> ItemsList = new ArrayList<String>();
    ArrayList<String> ItemsListBox = new ArrayList<String>();
    public ArrayList<String> ItemsName = new ArrayList<String>();
    public ArrayList<String> ItemsWeight = new ArrayList<String>();
    public ArrayList<String> ItemsPrice = new ArrayList<String>();

    private DatabaseReference itemsDB;

    private GoogleApiClient mGoogleApiClient;
    public static final int REQUEST_LOCATION_CODE = 99;
    private Context mContext;
    private Location lastLocation;
    private LocationRequest locationRequest;
    private Marker currentLocationMarker;

    private ImageView gasstation;


    private Button GetParking;
    Map<String, String> itemsMap;

    private int EnabledButton;
    //PLACES

    int PROXIMITY_RADIUS = 1500;
    int PROXIMITY_RADIUS_GASSTATION = 5000;
    double latitude, longitude;
    double end_latitude, end_longitude;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.mContext = context;
    }

    public Find() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_find, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //  latitude= 13.04089;
        //  longitude = 77.618386;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            getActivity().finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);

        getActivity().setTitle("Find a Parking");
        LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.ll);
        gasstation = (ImageView)getView().findViewById(R.id.gas_station);
        gasstation.setVisibility(View.VISIBLE);

        gasstation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sharedPrefManager = new SharedPrefManager(mContext);

                if (sharedPrefManager.getFuel().equalsIgnoreCase("get"))
                {
                    fuellocations();
                    gasstation.setBackgroundResource(R.color.orange);
                    sharedPrefManager.saveFuel(getActivity(),"save");
                }
                else
                {
                    gasstation.setBackgroundResource(R.color.colorgrey);
                    mMap.clear();
                    parkingLocations();
                    sharedPrefManager.saveFuel(getActivity(),"get");
                }


            }
        });
        GetParking = (Button) getView().findViewById(R.id.GetParking);
        GetParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Object dataTransfer[] = new Object[2];
//
                switch (v.getId()) {

                    case R.id.GetParking:
                        parkingLocations();

                        break;


                }


            }


        });
    }

    public void fuellocations()
    {

        Object dataTransfer[] = new Object[2];

        //mMap.clear();
        String gasstation = "petrolpump";
        String url = getUrlbyKeyword(latitude, longitude, gasstation);


        dataTransfer[0] = mMap;
        dataTransfer[1] = url;


        GetNearbyPlaceData getNearbyPlaceData = new GetNearbyPlaceData(new GetNearbyPlaceData.AsyncResponse() {
            @Override
            public void processFinish(final List<HashMap<String, String>> output) {
                Log.d("processFinish", "datarec");


            }
        });
        getNearbyPlaceData.execute(dataTransfer);
        // new GetNearbyPlaceData(this).execute(dataTransfer);
        //  Toast.makeText(getActivity(), "Showing nearby parking", Toast.LENGTH_LONG).show();


    }

    public void parkingLocations() {

        Object dataTransfer[] = new Object[2];
// Groceries
        mMap.clear();
        String parking = "parking";
        String grocery = "department_store,grocery_or_supermarket,store";
       // String url = getUrl(latitude, longitude, parking);
        String url =getUrlbyType(latitude, longitude, grocery);

        dataTransfer[0] = mMap;
        dataTransfer[1] = url;
        final LinearLayout linearLayout = getView().findViewById(R.id.ll);

        GetNearbyPlaceData getNearbyPlaceData = new GetNearbyPlaceData(
                new GetNearbyPlaceData.AsyncResponse() {
            @Override
            public void processFinish(final List<HashMap<String, String>> output) {
                Log.d("processFinish", "datarec");

                //  LinearLayout linearLayout = (LinearLayout)getView().findViewById(R.id.ll);


                final Button[] btn = new Button[output.size()];




                for (int i = 0; i < output.size(); i++) {
                    /*---------------Creating frame layout----------------------*/

                    final HashMap<String, String> googlePlace = output.get(i);
                    final String placeName = googlePlace.get("place_name");

                    FrameLayout frameLayout = new FrameLayout(getActivity());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                    frameLayout.setLayoutParams(layoutParams);


                 final    Button button = new Button(getActivity());
                    btn[i] = new Button(getActivity());

                    //create textview dynamically

                    button.setText(placeName);
                    btn[i].setText(placeName);
                    FrameLayout.LayoutParams lpText = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.CENTER);
                    // Note: LinearLayout.LayoutParams 's gravity was not working so I putted Framelayout as 3 paramater is gravity itself
                    //lpText.gravity = Gravity.BOTTOM | Gravity.CENTER;
//                    button.setTextColor(Color.parseColor("#000000"));
                    button.setTextColor(Color.parseColor("#1ABC9C"));
                    btn[i].setTextColor(Color.parseColor("#1ABC9C"));
                  //  button.setTextColor(Color.parseColor("#f86c5d"));
           //         button.setBackgroundResource(R.color.orange);

                 //   button.setBackgroundResource(R.drawable.linearlayout_buttonstyle);
                    button.setLayoutParams(lpText);
                    btn[i].setLayoutParams(lpText);

                    button.setId(i);
                    btn[i].setId(i);

                    final int index = i;



                   btn[i].setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                           for (int i = 0; i < output.size(); i++)
                           {
                               btn[i].setTextColor(Color.parseColor("#1ABC9C"));
                           }

                           btn[index].setTextColor(Color.parseColor("#f86c5d"));

                           final Activity activity= getActivity();
                         //  Intent i=new Intent(activity,OrderActivity.class);

                           getStoreItems(index);

                           Intent i=new Intent(activity,ItemsListActivity.class);
                           startActivity(i);



                           end_latitude = Double.parseDouble(googlePlace.get("lat"));
                           end_longitude = Double.parseDouble(googlePlace.get("lng"));

                           mMap.clear();
                           LatLng loc = new LatLng(end_latitude, end_longitude);
                           mMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkin_locationpin)).title(placeName));
                           Log.d("end_lat", "" + end_latitude);
                           Log.d("end_lng", "" + end_longitude);

                           if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                               // TODO: Consider calling

                               Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                               latitude = mLastLocation.getLatitude();
                               longitude = mLastLocation.getLongitude();
                               return;
                           }



                           Object dataTransfer[] = new Object[2];


                           dataTransfer = new Object[3];
                           String url = getDirectionsUrl();
                           GetDirectionsData getDirectionsData = new GetDirectionsData();
                           dataTransfer[0] = mMap;
                           dataTransfer[1] = url;
                           dataTransfer[2] = new LatLng(end_latitude, end_longitude);
                           getDirectionsData.execute(dataTransfer);
                           return;

                       }
                   });



                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Log.i("TAG", "The index is" + index);

                            button.setTextColor(Color.parseColor("#f86c5d"));



                            end_latitude = Double.parseDouble(googlePlace.get("lat"));
                            end_longitude = Double.parseDouble(googlePlace.get("lng"));

                            mMap.clear();
                            LatLng loc = new LatLng(end_latitude, end_longitude);
                            mMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromResource(R.mipmap.parkin_locationpin)).title(placeName));
                            Log.d("end_lat", "" + end_latitude);
                            Log.d("end_lng", "" + end_longitude);

                            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling

                                Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                latitude = mLastLocation.getLatitude();
                                longitude = mLastLocation.getLongitude();
                                return;
                            }



                            Object dataTransfer[] = new Object[2];


                            dataTransfer = new Object[3];
                            String url = getDirectionsUrl();
                            GetDirectionsData getDirectionsData = new GetDirectionsData();
                            dataTransfer[0] = mMap;
                            dataTransfer[1] = url;
                            dataTransfer[2] = new LatLng(end_latitude, end_longitude);
                            getDirectionsData.execute(dataTransfer);

                            // Add the order page here


                            return;
                        }


                    });
                    //Adding views at appropriate places

                    frameLayout.addView(btn[i]);
                    addRetailersToFirebase(index,placeName);
                    linearLayout.addView(frameLayout);

                }



            }
        });
        getNearbyPlaceData.execute(dataTransfer);



    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;
        } else
            return true;

    }


    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(mContext);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                                    .addConnectionCallbacks(this)
                                    .addOnConnectionFailedListener(this)
                                    .addApi(LocationServices.API)
                                    .build();

                            mGoogleApiClient.connect();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else// Permission is denied
                {
                    Toast.makeText(mContext, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                return;
        }
    }

    @Override
    public void onStart() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        super.onStart();
    }

    public void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationRequest = new LocationRequest();

        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        // Add a marker in Sydney and move the camera
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
if(mLastLocation==null)
{
    Toast.makeText(getActivity(), "Location is off", Toast.LENGTH_LONG).show();
}
else {
    LatLng here = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    mMap.addMarker(new MarkerOptions().position(here).title("YOU ARE HERE").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 14));

    latitude = mLastLocation.getLatitude();
    longitude = mLastLocation.getLongitude();

    parkingLocations();
}

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        lastLocation = location;
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        if (currentLocationMarker != null) {
            currentLocationMarker.remove();

        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Location");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        // markerOptions.icon(BitmapDescriptorFactory.fromResource(BitmapDescriptorFactory.HUE_BLUE));


        currentLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(10));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);


        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

        end_latitude = marker.getPosition().latitude;
        end_longitude = marker.getPosition().longitude;

        Log.d("end_lat", "" + end_latitude);
        Log.d("end_lng", "" + end_longitude);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mGoogleApiClient.connect();

            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            mMap.setMyLocationEnabled(true);
         //   mMap.setTrafficEnabled(true);
        }
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    private String getDirectionsUrl() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return null;
        }

        StringBuilder googleDirectionsUrl = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        googleDirectionsUrl.append("origin=" + latitude + "," + longitude);
        googleDirectionsUrl.append("&destination=" + end_latitude + "," + end_longitude);
        googleDirectionsUrl.append("&key=" + "AIzaSyBXltARx-ZGWq71njmQ_dUJQGrdq6gC0m8");


        return googleDirectionsUrl.toString();

    }

    private String getUrl(double latitude, double longitude, String nearbyPlace) {


        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location" + "=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type=" + nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=" + "AIzaSyBXltARx-ZGWq71njmQ_dUJQGrdq6gC0m8");

        Log.d("getUrl", "getURL Called");
        Log.d("getUrl data", String.valueOf(googlePlaceUrl));
        return googlePlaceUrl.toString();
    }

    private String getUrlbyKeyword(double latitude, double longitude, String nearbyPlace) {


        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location" + "=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + 3000);
        googlePlaceUrl.append("&keyword=" + nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key=" + "AIzaSyBXltARx-ZGWq71njmQ_dUJQGrdq6gC0m8");

        Log.d("getUrl", "getURL Called");
        Log.d("getUrl data", String.valueOf(googlePlaceUrl));
        return googlePlaceUrl.toString();
    }

    private String getUrlbyType(double latitude, double longitude, String nearbyPlace) {

        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location" + "=" + latitude + "," + longitude);
        googlePlaceUrl.append("&radius=" + 1500);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&type=" + nearbyPlace);
        googlePlaceUrl.append("&key=" + "AIzaSyBXltARx-ZGWq71njmQ_dUJQGrdq6gC0m8");
        googlePlaceUrl.append("&keyword=" + "Provision");

        Log.d("getUrl", "getURL Called");
        Log.d("getUrl data", String.valueOf(googlePlaceUrl));
        return googlePlaceUrl.toString();
    }


    private void addRetailersToFirebase(int index, String placeName)
    {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        DatabaseReference current_retailer_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Retailer").child(user_id+String.valueOf(index));
        current_retailer_db.child("RID").setValue(String.valueOf(index));
        current_retailer_db.child("StoreName").setValue(String.valueOf(placeName));
    }

    private void getStoreItems(int index)
    {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        itemsDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Retailer").child(user_id+String.valueOf(index)).child("Items");
        itemsDB.orderByChild("Items").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                ItemsList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    Map<String, String> map = (Map) snapshot.getValue();
                    if (map != null) {
                        String NameItem = snapshot.getKey()+ ",";
                        String Price = map.get("Price");
                        String Weight = map.get("Weight");

                    }
                    String NameItem = snapshot.getKey()+ ",";
                   // itemsMap[snapsot.getKey()]= snapshot.getValue();
                    // String x = snapshot.getKey();
                    String entry = String.valueOf(snapshot.getValue());
                    String box = NameItem.concat(entry);


                    ItemsList.add(entry);
                    ItemsListBox.add(box);

                    ArrayList<String> addHistoryList = new ArrayList<String>();

                    Log.d("Items", entry);
                }

                hPreferences = mContext.getSharedPreferences("SaveItems",Context.MODE_PRIVATE);
                Set<String> items = new HashSet<String>();
                Set<String> itemsBox = new HashSet<String>();


                SharedPreferences.Editor editor= hPreferences.edit();

                items.addAll(ItemsList);
                itemsBox.addAll(ItemsListBox);
                editor.putStringSet("items",items);
                editor.putStringSet("itemsBox",itemsBox);

                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}



