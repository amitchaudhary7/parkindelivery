package mobi.parkin.android.parkindelivery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.Result;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class Scanner extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    public  static boolean scanned ;


    private static final int REQUEST_CAMERA=1;
    private ZXingScannerView scannerView;
    private Task<Void> mCustomerDatabase;
    private FirebaseAuth mAuth;
    private  String userID;
    public static long checkintime ;
    public static long rate;
    public static String  CarCharge;
    public static String  BikeCharge;
    public static String  HourlyCharge;
    public static String  ParkingCoordinates;

    public SharedPrefManager sharedPrefManager;
    private final Context mContext = this;

    public static String car,bike, basepriceValid,coordinates, checkoutBeforeTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

       // scannerView = new ZXingScannerView(this);
       // setContentView(scannerView);

        scannerView = (ZXingScannerView)findViewById(R.id.zxscan);



        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if (checkPermission())
            {
                Toast.makeText(Scanner.this,"Permission is granted!",Toast.LENGTH_LONG).show();
            }

            else
            {
                requestPermission();
            }
        }
    }

    private boolean checkPermission()
    {

        return (ContextCompat.checkSelfPermission(Scanner.this, CAMERA)== PackageManager.PERMISSION_GRANTED);
    }

    private void  requestPermission()
    {
        ActivityCompat.requestPermissions(this,new String[]{CAMERA},REQUEST_CAMERA);
    }


    public void onRequestPermissionResult(int requestCode, String permission[],int grantResults[])
    {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(Scanner.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Scanner.this, "Permission Denied", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                displayAlertMessage("You need to allow acces for both application", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                                        }
                                    }
                                });

                                return;
                            }
                        }
                    }
                }
                break;
        }

    }

    public void onResume()
    {
        super.onResume();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M)
        {
            if (checkPermission())
            {
                if(scannerView==null)
                {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);


                }
                scannerView.setResultHandler(this);
                scannerView.startCamera(); // Camera starts here

            }
            else
            {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener)
    {
        new AlertDialog.Builder(Scanner.this)
                .setMessage(message)
                .setPositiveButton("Ok",listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();


    }

    @Override
    public void handleResult(Result result) {



        //Storing CheckIn Time*******************************************
        // mAuth = FirebaseAuth.getInstance();
        //  userID = mAuth.getCurrentUser().getUid();
        //Map<String, Object> checkinTime = new HashMap<>();
        // checkinTime.put("checkinTime", ServerValue.TIMESTAMP);

        // checkintime = mainActivity.getTrueTime().getTime();
        checkintime = MainActivity.getTrueTime().getTime();

        Log.d("CheckIn", String.valueOf(checkintime));

        //   mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).updateChildren(checkinTime);
        //******************************************************************
        //String str= "car-50-20,bike-30-40,coordinates-MantriMall,checkoutBeforeTime-900,basepriceValid-7200";// Example string

        final String scanResult = result.getText();
        String[] arrOfStr = scanResult.split(",");

        for(String a: arrOfStr)
        {
            // System.out.println(a);
            String[] arrOfSubStr = a.split("-");

            for(String b: arrOfSubStr)
            {
                //	System.out.println(b);
                if(b.equalsIgnoreCase("Car"))
                {
                    car = arrOfSubStr[1]+","+arrOfSubStr[2];
                    //System.out.println(car);
                }
                else if(b.equalsIgnoreCase("bike"))
                {
                    bike = arrOfSubStr[1]+","+arrOfSubStr[2];
                    //System.out.println(bike);
                }
                else if(b.equalsIgnoreCase("coordinates"))
                {
                    coordinates= arrOfSubStr[1];
                    //System.out.println(coordinates);
                }
                else if(b.equalsIgnoreCase("checkoutBeforeTime"))
                {
                    checkoutBeforeTime= arrOfSubStr[1];
                    // System.out.println(checkoutBeforeTime);
                }
                else if(b.equalsIgnoreCase("basepriceValid"))
                {
                    basepriceValid= arrOfSubStr[1];
                    // System.out.println(basepriceValid);
                }


            }

        }

        mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);
      //  DatabaseReference current_client_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Client").child(coordinates);

        current_user_db.child("CurrentParkingInformation").child("basePriceValid").setValue(basepriceValid);
        current_user_db.child("CurrentParkingInformation").child("bike").setValue(bike);
        current_user_db.child("CurrentParkingInformation").child("car").setValue(car);
        current_user_db.child("CurrentParkingInformation").child("checkOutBeforeTime").setValue(checkoutBeforeTime);
        current_user_db.child("CurrentParkingInformation").child("coordinates").setValue(coordinates);
        // current_user_db.child("CurrentParkingInformation").child("number").setValue(personGivenName);
        //current_user_db.child("CurrentParkingInformation").child("type").setValue(personGivenName);

        //*******************//

        Date trueTime = MainActivity.getTrueTime();
        String checkinTimeinms= String.valueOf(MainActivity.getTrueTime().getTime());
        String checkin_time =_formatDate(trueTime, "yyyy-MM-dd HH:mm:ss", TimeZone.getTimeZone("GMT+05:30"));

        scanned = true;
        String state = "scanned";
        sharedPrefManager = new SharedPrefManager(mContext);
        sharedPrefManager.saveState(this, state);

        sharedPrefManager.saveBasePriceValidity(this, basepriceValid);
        sharedPrefManager.saveBikePrices(this, bike);
        sharedPrefManager.saveCarPrices(this, car);
        sharedPrefManager.saveCountdownTime(this, checkoutBeforeTime);
        sharedPrefManager.saveCoordinates(this, coordinates);
        sharedPrefManager.saveTimeIn(this, checkin_time);
        sharedPrefManager.savecheckInTimeinms(this, checkinTimeinms);




        String numbrplate = sharedPrefManager.getSelectedVehNum();
        String vtyp = sharedPrefManager.getSelectedVehType();

//        Bundle args= new Bundle();
//        args.putString("value1",state);
//        Pay.putArguments(args);

      //  Pay pay = new Pay();
        if (sharedPrefManager.getFragment().equalsIgnoreCase("pay"))
        { Pay.updateTextViews(coordinates,numbrplate,vtyp,bike,car,basepriceValid);}


        Pay.orangepark();

        Intent intent = new Intent(this, ParkedLocation.class);
        finish();
        startActivity(intent);




    }


    public static  String _formatDate(Date date, String pattern, TimeZone timeZone) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        format.setTimeZone(timeZone);
        return format.format(date);
    }

    public static long HourlyCharge()
    {
        if (HourlyCharge!=null)
            return Long.parseLong(HourlyCharge);
        else {
            //Toast.makeText(this, "Car added ", Toast.LENGTH_LONG).show();
            //TOAST that not checked in anywhere
            return 3;
        }
    }

    public static long CarCharge()
    {
        return Long.parseLong(CarCharge);
    }
    public static long BikeCharge()
    {
        return Long.parseLong(BikeCharge);
    }



}
