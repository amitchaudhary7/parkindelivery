package mobi.parkin.android.parkindelivery;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryAdapter extends ArrayAdapter<History> {

    Context context;
    int layoutResourceId;
    History data[] = null;

    public HistoryAdapter(Context context, int layoutResourceId, History[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        HistoryAdapter.HistoryHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new HistoryAdapter.HistoryHolder();
            holder.imgIcons = (ImageView)row.findViewById(R.id.imgIcons);
            holder.title = (TextView)row.findViewById(R.id.title);
            holder.cost = (TextView)row.findViewById(R.id.cost);
            holder.timein = (TextView)row.findViewById(R.id.timein);
            holder.vehicle = (TextView)row.findViewById(R.id.vehicle);
            holder.timeout = (TextView)row.findViewById(R.id.timeout);
            row.setTag(holder);
        }
        else
        {
            holder = (HistoryAdapter.HistoryHolder)row.getTag();
        }

        History history = data[position];
        holder.imgIcons.setImageResource(history.icon);
        holder.title.setText(history.title);
        holder.cost.setText(history.cost);
        holder.timein.setText(history.timein);
        holder.vehicle.setText(history.vehicle);
        holder.timeout.setText(history.timeout);



        return row;
    }

    static class HistoryHolder
    {
        ImageView imgIcons;
        TextView title;
        TextView cost;
        TextView timein;
        TextView timeout;
        TextView vehicle;
    }
}

