package mobi.parkin.android.parkindelivery;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Set;

public class VehicleListSwipeView extends AppCompatActivity {

    public static boolean car = true;
    public static boolean bike;
    SharedPreferences hPreferences;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    Dialog myDialog;
    private DatabaseReference vehicleDB;
    private  String userID;
    ArrayList<String> VehList = new ArrayList<String>();

    public SharedPrefManager sharedPrefManager;

    public static String SelectedVehnum , SelectedVehType;
    private static final String TAG = "VehicleListSwipeView";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_list_swipe_view);
        final SwipeMenuListView swipeMenuListView = (SwipeMenuListView)findViewById(R.id.swipelistview);
        View header = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);
        swipeMenuListView.addHeaderView(header);

        swipeMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //HashMap<String,String> o = (HashMap<String, String>)listView1.getItemAtPosition(i);

                Log.d("car is", String.valueOf(i));
                Vehicle item = (Vehicle) adapterView.getItemAtPosition(i);
                //       Log.d("car num is", String.valueOf(item.title));
//if (item.title!=null && item.title.equals(0)) {
                if(item!=null)
                {    if (item.title!=null)
                SelectedVehnum = String.valueOf(item.title);

                Log.d("car icon is", String.valueOf(item.icon));

                SelectedVehType = String.valueOf(item.icon);}
//}

                //  sharedPrefManager.saveEmail(this, String.valueOf(item.icon));
//                Pay.VehicleTypeSelected.setImageIcon(R.mipmap.car);
//                Pay.VehSelected.setText(String.valueOf(item.title));
//                Pay.VehSelected.setVisibility(View.VISIBLE);
            }
        });


        SharedPreferences VehicleHistory = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
        Set<String> Car = VehicleHistory.getStringSet("car",null);
        Set<String> Bike = VehicleHistory.getStringSet("bike",null);
        ArrayList<String> carList = new ArrayList<String>();
        ArrayList<String> bikeList = new ArrayList<String>();
        TextView txtAdd;
        txtAdd= (TextView)findViewById(R.id.addVehicle) ;

        myDialog = new Dialog(this);

        if (Car!=null) {
            for (String x : Car) {
                carList.add(x);
            }
        }

        if (Bike!=null) {
            for (String y : Bike) {
                bikeList.add(y);
            }
        }

        int x=carList.size()+bikeList.size();
        int j=0, z=0;
        final Vehicle[] vehicles_data = new Vehicle[x];
        for (int i=0; i<x;i++) {
            if (carList.size()!=0 && i<carList.size())
                vehicles_data[i] = new Vehicle(R.mipmap.parkin_car, carList.get(z++));
            else
                vehicles_data[i] = new Vehicle(R.mipmap.parkin_bike, bikeList.get(j++));
        }





        VehicleAdapter adapter = new VehicleAdapter(this,
                R.layout.listview_item_row, vehicles_data);

        swipeMenuListView.setAdapter(adapter);


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(248, 108,
                        93)));
                // set item width
                openItem.setWidth(280);
                // set item title
                openItem.setTitle("Delete");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

            }
        };

        // set creator
        swipeMenuListView.setMenuCreator(creator);



        swipeMenuListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        Log.d(TAG, "onMenuItemClick: clicked item"+ index);
                        Log.d(TAG, "onMenuItemClick: pos clicked item"+ position);

                        Vehicle m = vehicles_data[position];
                        String n = m.title;
                        int o = m.icon;

                        hPreferences = getSharedPreferences("SaveVehicle",Context.MODE_PRIVATE);
                        if (o==2131492905) {
                            Set<String> bike = hPreferences.getStringSet("bike", null);
                            Set<String> car = hPreferences.getStringSet("car",null);
                            SharedPreferences.Editor editor = hPreferences.edit();
                            // Set<String> bike = new HashSet<String>();
                            editor.clear();
                            bike.remove(n);
                            editor.putStringSet("bike", bike);
                            editor.putStringSet("car", car);
                            editor.commit();
                            editor.apply();

                            ArrayList<String> addBikeList = new ArrayList<String>();
                            if (bike!=null) {
                                for (String x : bike) {
                                    addBikeList.add(x);
                                }
                            }
                            mAuth = FirebaseAuth.getInstance();
                            userID = mAuth.getCurrentUser().getUid();
                            vehicleDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles");

                            vehicleDB.child("Bike").setValue(addBikeList);
                        }
                        else if (o==2131492906)
                        {
                            Set<String> car = hPreferences.getStringSet("car", null);
                            Set<String> bike = hPreferences.getStringSet("bike", null);
                            SharedPreferences.Editor editor = hPreferences.edit();
                            // Set<String> bike = new HashSet<String>();
                            editor.clear();
                            car.remove(n);
                            editor.putStringSet("car", car);
                            editor.putStringSet("bike", bike);
                            editor.commit();
                            editor.apply();

                            ArrayList<String> addCarList = new ArrayList<String>();
                            if (car!=null) {
                                for (String x : car) {
                                    addCarList.add(x);
                                }
                            }
                            mAuth = FirebaseAuth.getInstance();
                            userID = mAuth.getCurrentUser().getUid();
                            vehicleDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles");

                            vehicleDB.child("Car").setValue(addCarList);
                        }
                        finish();
                        startActivity(getIntent());
                        break;
                    case 1:

                        Log.d(TAG, "onMenuItemClick: clicked item"+ index);
                        Log.d(TAG, "onMenuItemClick: pos clicked item"+ position);


                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


        txtAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopup();
            }
        });

    }




    public void ShowPopup()
    {

        car = false;
        bike = false;


        TextView txtclose;
        Button btnAdd;
        RadioGroup radioGroup;
        final EditText VehicleInputField;
        myDialog.setContentView(R.layout.enter_vehicle_number);
        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        btnAdd = (Button) myDialog.findViewById(R.id.buttonAdd);
        radioGroup = (RadioGroup) myDialog.findViewById(R.id.toggle);
        VehicleInputField = (EditText)myDialog.findViewById(R.id.VehicleInput);



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId == R.id.Car) {
                    //do work when radioButton1 is active

                    car = true;
                    bike = false;
                    Log.d("car is", String.valueOf(car));
                } else  if (checkedId == R.id.Bike) {
                    //do work when radioButton2 is active

                    car = false;
                    bike = true;


                    Log.d("bike is", String.valueOf(bike));
                }
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //        mAuth = FirebaseAuth.getInstance();
                //      userID = mAuth.getCurrentUser().getUid();
                //    vehicleDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles");

                if (car)
                {
                    mAuth = FirebaseAuth.getInstance();
                    userID = mAuth.getCurrentUser().getUid();
                    vehicleDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles");

                    String carNumber;
                    carNumber= VehicleInputField.getText().toString();

                    // vehicleDB.child("Car").child(carNumber).setValue(true);

                    ArrayList<String> addCarList = new ArrayList<String>();

                    //bug here


                    VehList.add(carNumber);
                    hPreferences = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
                    Set<String> car = hPreferences.getStringSet("car",null);
                    Set<String> bike = hPreferences.getStringSet("bike",null);


                    SharedPreferences.Editor editor= hPreferences.edit();
                    //  Set<String> car = new HashSet<String>();
                    editor.clear();
                    car.addAll(VehList);
                    // vehicleDB.child("Car").setValue(car);
                    if (car!=null) {
                        for (String x : car) {
                            addCarList.add(x);
                        }
                    }
                    vehicleDB.child("Car").setValue(addCarList);
                    editor.putStringSet("car",car);
                    editor.putStringSet("bike",bike);
                    editor.commit();
                    editor.apply();

////////////////////////////////
                    Toast.makeText(getApplicationContext(), "Car added ", Toast.LENGTH_LONG).show();
                    finish();
                    startActivity(getIntent());
                }
                else if (bike)
                {
                    mAuth = FirebaseAuth.getInstance();
                    userID = mAuth.getCurrentUser().getUid();
                    vehicleDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles");

                    String bikeNumber;
                    bikeNumber= VehicleInputField.getText().toString();

                    ArrayList<String> addBikeList = new ArrayList<String>();
                    //   vehicleDB.child("Bike").setValue(bikeNumber);
                    //  Toast.makeText(getApplicationContext(), "Car added ", Toast.LENGTH_LONG).show();
                    //   vehicleDB.child("Bike").child(bikeNumber).setValue(true);



                    VehList.add(bikeNumber);
                    hPreferences = getSharedPreferences("SaveVehicle",Context.MODE_PRIVATE);
                    Set<String> bike = hPreferences.getStringSet("bike",null);
                    Set<String> car = hPreferences.getStringSet("car",null);
                    SharedPreferences.Editor editor= hPreferences.edit();
                    // Set<String> bike = new HashSet<String>();
                    editor.clear();

                    bike.addAll(VehList);
                    if (bike!=null) {
                        for (String x : bike) {
                            addBikeList.add(x);
                        }
                    }
                    vehicleDB.child("Bike").setValue(addBikeList);
                    editor.putStringSet("bike",bike);
                    editor.putStringSet("car",car);
                    editor.commit();
                    editor.apply();

                    finish();
                    startActivity(getIntent());
                    Toast.makeText(getApplicationContext(), "Bike added ", Toast.LENGTH_LONG).show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Select Car/Bike ", Toast.LENGTH_LONG).show();

                }
                bike= false;
                car =false;
            }
        });

        txtclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }
}
