package mobi.parkin.android.parkindelivery;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VehicleAdapter  extends ArrayAdapter<Vehicle> {

    Context context;
    int layoutResourceId;
    Vehicle data[] = null;

    public VehicleAdapter(Context context, int layoutResourceId, Vehicle[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        VehicleAdapter.VehicleHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new VehicleAdapter.VehicleHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);

            row.setTag(holder);
        }
        else
        {
            holder = (VehicleAdapter.VehicleHolder)row.getTag();
        }

        Vehicle vehicle = data[position];
        holder.txtTitle.setText(vehicle.title);
        holder.imgIcon.setImageResource(vehicle.icon);




        return row;
    }

    static class VehicleHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
