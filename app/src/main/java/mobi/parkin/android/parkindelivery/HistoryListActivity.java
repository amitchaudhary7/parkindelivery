package mobi.parkin.android.parkindelivery;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class HistoryListActivity extends AppCompatActivity {

    // private ListView listViewhistory;


    SharedPreferences hPreferences;
    private String userID;

    public String title, timein, timeout;

    public String vehicle,numPlate;
    public String cost;


    ArrayList<String> VehList = new ArrayList<String>();
    private FirebaseAuth mAuth;
    public SharedPrefManager sharedPrefManager;

    private DatabaseReference historyDB;
    ArrayList<String> HistoryList = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list);

        final SwipeMenuListView listViewhistory = (SwipeMenuListView) findViewById(R.id.historyswipelistview);
        View header = (View) getLayoutInflater().inflate(R.layout.history_header_row, null);
        listViewhistory.addHeaderView(header);

        SharedPreferences VehicleHistory = getSharedPreferences("SaveHistory", Context.MODE_PRIVATE);
        Set<String> history = VehicleHistory.getStringSet("transaction", null);

        ArrayList<String> historyList = new ArrayList<String>();
        final ArrayList<String> historyTime = new ArrayList<String>();
        ArrayList<String> newhistoryList = new ArrayList<String>();


        if (history != null) {
            for (String x : history) {
                historyList.add(x);

                String[] y = x.split(",");
                // String[] w = y[6].split("=");
                String[] w = new String[0];
                for (int i = 0; i < y.length; i++) {
                    if (y[i].toLowerCase().contains("time_in")) {
                        //  String[] w = y[i].split("=");
                        w = y[i].split("=");
                    }
                }
                historyTime.add(w[1]);

                Log.d("timeout_new", "timeot" + w[1]);

//                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
//                try {
//                    Date mDate = sdf.parse(w[1]);
//                    long timeInMilliseconds = mDate.getTime();
//                //    System.out.println("Date in milli :: " + timeInMilliseconds);
//                    Log.d("timeout_new", "timeinms" + timeInMilliseconds);
//
//                    long currenttime = MainActivity.getTrueTime().getTime();
//                    long d = currenttime-timeInMilliseconds;
//                    Log.d("time_diif", "time_diif" + d);
//
//                    historyTime.add( d);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
            }
        }

        if (historyTime!=null) {
            Collections.sort(historyTime, Collections.<String>reverseOrder());
            for (String z : historyTime) {

                for (String y :historyList)
                {
                    if (y.contains(z))
                    {
                        newhistoryList.add(y);
                    }
                }
            }
        }

        int x = historyList.size();

        final History[] history_data = new History[x];
        for (int i = 0; i < x; i++) {

            final String rawString = newhistoryList.get(i);
            String[] arrOfStrs = rawString.split(",");

            for (String a : arrOfStrs) {
                // System.out.println(a);
                String[] arrOfSubStrs = a.split("=");

                //     for(String b: arrOfSubStrs)
                //     {
                //	System.out.println(b);
                if (arrOfSubStrs[0].equalsIgnoreCase(" parkingName")) {
                    title = arrOfSubStrs[1];

                } else if (arrOfSubStrs[0].equalsIgnoreCase(" cost")) {
                    cost = "Rs."+ arrOfSubStrs[1];

                } else if (arrOfSubStrs[0].equalsIgnoreCase(" time_in")) {
                    timein = arrOfSubStrs[1];
                    //  timein =   timein.substring(0, timein.length() - 14);

                } else if (arrOfSubStrs[0].equalsIgnoreCase(" time_out")) {
                    timeout = arrOfSubStrs[1];
                    timeout= timeout.substring(0, timeout.length() - 1);


                } else if (arrOfSubStrs[0].equalsIgnoreCase(" type")) {
                    vehicle = arrOfSubStrs[1];

                }
                else if (arrOfSubStrs[0].equalsIgnoreCase(" type")) {
                    vehicle = arrOfSubStrs[1];

                }
                else if (arrOfSubStrs[0].equalsIgnoreCase(" vehicle_Number")) {
                    numPlate = arrOfSubStrs[1];
                    //   numPlate =   numPlate.substring(0, numPlate.length() - 1);

                }


            }
            if (newhistoryList.size() != 0 && i < newhistoryList.size()) {

                if (vehicle.equalsIgnoreCase("bike"))
                {   history_data[i] = new History(R.mipmap.parkin_bike, title, cost, timein, timeout, numPlate);}
                else {
                    history_data[i] = new History(R.mipmap.parkin_car, title, cost, timein, timeout, numPlate);

                }
            }

        }
        HistoryAdapter adapter = new HistoryAdapter(this,
                R.layout.historylistview_item_row, history_data);



        listViewhistory.setAdapter(adapter);
        adapter.notifyDataSetChanged();



        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(248, 108,
                        93)));
                // set item width
                openItem.setWidth(280);
                // set item title
                openItem.setTitle("Delete");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

            }
        };



        listViewhistory.setMenuCreator(creator);

        listViewhistory.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index){
                    case 0:
                        Log.d("x", "onMenuItemClick: clicked item"+ index);
                        Log.d("x", "onMenuItemClick: pos clicked item"+ position);

                        History m = history_data[position];
                        String n = m.title;
                        int o = m.icon;
                        String p = m.cost;
                        String q = m.timein;
                        String r = m.timeout;
                        String s = m.vehicle;


                        mAuth = FirebaseAuth.getInstance();
                        String user_id= mAuth.getCurrentUser().getUid();
                        DatabaseReference current_parking_history = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory").child(r);
                        current_parking_history.removeValue();

                        finish();
                        startActivity(getIntent());

                        deleteHistory();
                        break;


                    case 1:




                        break;
                }
                return false;
            }
        });

    }


    public void deleteHistory()
    {
        String userID = mAuth.getCurrentUser().getUid();
        historyDB = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("UserParkingHistory");
        historyDB.orderByChild("UserParkingHistory").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                HistoryList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    // String x = snapshot.getKey();
                    String entry = String.valueOf(snapshot.getValue());


                    HistoryList.add(entry);

                    ArrayList<String> addHistoryList = new ArrayList<String>();



                    //  Log.d("x", x);
                    Log.d("Histry", entry);
                }

                hPreferences = getSharedPreferences("SaveHistory",Context.MODE_PRIVATE);
                //Set<String> transaction = hPreferences.getStringSet("transaction",null);
                Set<String> transaction = new HashSet<String>();
                SharedPreferences.Editor editor= hPreferences.edit();

                transaction.addAll(HistoryList);
//                    if (transaction!=null) {
//                        for (String x : transaction) {
//                            addHistoryList.add(x);
//                        }
//                    }
                editor.putStringSet("transaction",transaction);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}

