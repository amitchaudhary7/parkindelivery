package mobi.parkin.android.parkindelivery;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

public class Payment extends AppCompatActivity implements PaymentResultListener {

    Pay Pay= new Pay();
    Scanner scanner = new Scanner();

    EditText value;
    Button pay;
    int payamount;
    TextView Bill;

    public SharedPrefManager sharedPrefManager;
    private final Context mContext = this;


    public  long time;
    public  long charge;
    public  long billAmount;
    public  String cost;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        value = (EditText)findViewById(R.id.input);
        pay= (Button) findViewById(R.id.razorpay);

        Bill = (TextView)findViewById(R.id.bill) ;


        charge = BillAmount();
        cost = String.valueOf(charge);



        value.setText(cost);
        Bill.setText(cost);

        //  value.setText("19");
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPayment();
            }
        });
//time =threeFragment.difference;

    }

    private void startPayment()
    {
        // payamount=Integer.parseInt(value.getText().toString());

        payamount=Integer.parseInt(cost);


        Checkout checkout = new Checkout();

        checkout.setImage(R.mipmap.card);//logo
        final Activity activity= this;

        try{

            JSONObject options = new JSONObject();
            options.put("description", "Order #007");

            options.put("currency","INR");

            options.put("amount",payamount*100);

            checkout.open(activity,options);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPaymentSuccess(String s) {


    //   Toast.makeText(Payment.this, "PAYMENT SUCCESSFUL",Toast.LENGTH_SHORT).show();
        Toast.makeText(Payment.this, s, Toast.LENGTH_LONG).show();

        mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        String time_out = String.valueOf(MainActivity.getTrueTime());
        DatabaseReference current_parking =FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("CurrentParkingInformation");
        current_parking.removeValue();

        DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory").child(time_out);

        current_user_db.child("basePriceValid").setValue(sharedPrefManager.getBasePriceValid());
        current_user_db.child("checkOutBeforeTime").setValue(sharedPrefManager.getCheckoutBeforeTime());
        current_user_db.child("parkingName").setValue(sharedPrefManager.getCoordinates());
        current_user_db.child("cost").setValue(billAmount);
        current_user_db.child("time_in").setValue(sharedPrefManager.getTime_in());
        current_user_db.child("time_out").setValue(time_out);
        current_user_db.child("type").setValue(sharedPrefManager.getSelectedVehType());
        current_user_db.child("vehicle_Number").setValue(sharedPrefManager.getSelectedVehNum());

        DatabaseReference history_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory");





    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(Payment.this, "PAYMENT FAILED",Toast.LENGTH_SHORT).show();
    }


    public  long BillAmount()
    {
        sharedPrefManager = new SharedPrefManager(mContext);

        String x= sharedPrefManager.getSelectedVehType();
        if ( x.equalsIgnoreCase("car"))
        {
            // billAmount = Scanner.CarCharge()* Pay.getduration();

            String str = sharedPrefManager.getCarPrices();
            String[] arrStr = str.split(",");
            long CarBase = Long.parseLong(arrStr[0]);
            long CarAdOn = Long.parseLong(arrStr[1]);
            long BaseValidity = Long.parseLong(sharedPrefManager.getBasePriceValid())/3600;

            if (Pay.getduration()>BaseValidity)
            {billAmount = CarBase+ CarAdOn* (Pay.getduration()-BaseValidity);}
            else
            {
                billAmount = CarBase;
            }
        }
        else if (x.equalsIgnoreCase("bike"))
        {
            String str2 = sharedPrefManager.getBikePrices();
            String[] arrStr2 = str2.split(",");
            long BikeBase = Long.parseLong(arrStr2[0]);
            long BikeAdOn = Long.parseLong(arrStr2[1]);
            long BaseValidity = Long.parseLong(sharedPrefManager.getBasePriceValid())/3600;

            //billAmount = BikeBase+ BikeAdOn* Pay.getduration();
            if (Pay.getduration()>BaseValidity)
            {billAmount = BikeBase+ BikeAdOn* (Pay.getduration()-BaseValidity);}
            else
            {
                billAmount = BikeBase;
            }
        }

        String state = "paid";
        sharedPrefManager = new SharedPrefManager(mContext);
        sharedPrefManager.saveState(this, state);


        Pay.redpark();

        return billAmount;
    }

}

