package mobi.parkin.android.parkindelivery;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.instacart.library.truetime.TrueTime;
import com.razorpay.PaymentResultListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,PaymentResultListener {

    SharedPreferences.Editor fd;
    SharedPreferences FeedPref;
    ArrayList<String> VehList = new ArrayList<String>();
    SharedPreferences hPreferences;


    private DatabaseReference vehicleDBCar;
    private DatabaseReference vehicleDBBike;

    public static final int REQUEST_LOCATION_CODE = 99;

    public static String vehSelectedType, vehNumSelected;

    public static GoogleApiClient mGoogleApiClient;

    String TAG = "DBG";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    Dialog myDialog;


    private final static float CLICK_DRAG_TOLERANCE = 10; // Often, there will be a slight, unintentional, drag when the user taps the FAB, so we need to account for this.

    private float downRawX, downRawY;
    private float dX, dY;

    public static boolean car = true;
    public static boolean bike;


    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.mipmap.find,
            R.mipmap.park,
            R.mipmap.pay
    };

    SharedPrefManager sharedPrefManager;
    private String mUsername, mEmail;
    Context mContext = this;
    private TextView mFullNameTextView, mEmailTextView;
    private ImageView mProfileImageView;

    private DatabaseReference vehicleDB;
    private  String userID;


    HorizontalScrollMenuView menu;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  setSupportActionBar(toolbar);
      //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        initTrueTime(this);



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        //navigation.setItemIconTintList(null);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


       loadFragment(new Find());
        sharedPrefManager = new SharedPrefManager(mContext);
        sharedPrefManager.saveFragment(mContext, "find");
        sharedPrefManager.saveFuel(mContext,"get");


        //create menu



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();



            }
        });


        fab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                int action = motionEvent.getAction();
                if (action == MotionEvent.ACTION_DOWN) {

                    downRawX = motionEvent.getRawX();
                    downRawY = motionEvent.getRawY();
                    dX = view.getX() - downRawX;
                    dY = view.getY() - downRawY;

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_MOVE) {

                    int viewWidth = view.getWidth();
                    int viewHeight = view.getHeight();

                    View viewParent = (View)view.getParent();
                    int parentWidth = viewParent.getWidth();
                    int parentHeight = viewParent.getHeight();

                    float newX = motionEvent.getRawX() + dX;
                    newX = Math.max(0, newX); // Don't allow the FAB past the left hand side of the parent
                    newX = Math.min(parentWidth - viewWidth, newX); // Don't allow the FAB past the right hand side of the parent

                    float newY = motionEvent.getRawY() + dY;
                    newY = Math.max(0, newY); // Don't allow the FAB past the top of the parent
                    newY = Math.min(parentHeight - viewHeight, newY); // Don't allow the FAB past the bottom of the parent

                    view.animate()
                            .x(newX)
                            .y(newY)
                            .setDuration(0)
                            .start();

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_UP) {

                    float upRawX = motionEvent.getRawX();
                    float upRawY = motionEvent.getRawY();

                    float upDX = upRawX - downRawX;
                    float upDY = upRawY - downRawY;

//                    float upDX = upRawX - view.getX();
//                    float upDY = upRawY - view.getY();



                    if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                        // scan();
                        popupSelectVehicle();

                        return true;
                    }
                    else { // A drag
                        return true; // Consumed
                    }

                }
                else {
                    return MainActivity.super.onTouchEvent(motionEvent);
                }

            }
        });



//        mUsername = sharedPrefManager.getName();


        myDialog = new Dialog(this);

        FeedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        fd =FeedPref.edit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        View header = navigationView.getHeaderView(0);

        mFullNameTextView = (TextView) header.findViewById(R.id.NameView);
        mEmailTextView = (TextView) header.findViewById(R.id.emailView);
        mProfileImageView = (ImageView) header.findViewById(R.id.imageView);
        // create an object of sharedPreferenceManager and get stored user data
        sharedPrefManager = new SharedPrefManager(mContext);
        mUsername = sharedPrefManager.getName();
        mEmail = sharedPrefManager.getUserEmail();
        String uri = sharedPrefManager.getPhoto();
        if (uri!=null)
        {  Uri mPhotoUri = Uri.parse(uri);

            Picasso.with(mContext)
                    .load(mPhotoUri)
                    .placeholder(android.R.drawable.sym_def_app_icon)
                    .error(android.R.drawable.sym_def_app_icon)
                    .into(mProfileImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Bitmap imageBitmap = ((BitmapDrawable) mProfileImageView.getDrawable()).getBitmap();
                            RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                            imageDrawable.setCircular(true);
                            imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                            mProfileImageView.setImageDrawable(imageDrawable);
                        }

                        @Override
                        public void onError() {
                            mProfileImageView.setImageResource(R.mipmap.default_profile_pic);
                        }
                    });


        }
        //  TextView name = (TextView)navigationView.getHeaderView(0).findViewById(R.id.textView);
        //  name.setText(mUsername);
        mFullNameTextView.setText(mUsername);
        mEmailTextView.setText(mEmail);



        sharedPrefManager = new SharedPrefManager(mContext);

        String x= sharedPrefManager.getState();
        sharedPrefManager.saveState(this, x);


    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_find:

                    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                    toolbar.setTitle("   Find a Store    ");
                    //toolbar.setTitle("Shop")

                    fragment = new Find();
                    loadFragment(fragment);
                    sharedPrefManager = new SharedPrefManager(mContext);
                    sharedPrefManager.saveFragment(mContext, "find");



                    return true;
                case R.id.navigation_park:

                     toolbar = (Toolbar) findViewById(R.id.toolbar);
                    toolbar.setTitle("   Get parked    ");

                    fragment = new Park();
                    loadFragment(fragment);
                    sharedPrefManager = new SharedPrefManager(mContext);
                    sharedPrefManager.saveFragment(mContext, "park");
                    return true;
                case R.id.navigation_pay:

                     toolbar = (Toolbar) findViewById(R.id.toolbar);
                    toolbar.setTitle("   Payment    ");

                    fragment = new Pay();
                    loadFragment(fragment);
                    sharedPrefManager = new SharedPrefManager(mContext);
                    sharedPrefManager.saveFragment(mContext, "pay");
                    return true;

                    default:
                        fragment = new Find();
                        loadFragment(fragment);
                        sharedPrefManager = new SharedPrefManager(mContext);
                        sharedPrefManager.saveFragment(mContext, "find");

            }
            return false;
        }
    };


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //*********TRUE TIME STARTS*************
    public static Date getTrueTime() {
        Date date = TrueTime.isInitialized() ? TrueTime.now() : new Date();
        return date;
    }

    public static void initTrueTime(Context ctx) {
        if (isNetworkConnected(ctx)) {
            if (!TrueTime.isInitialized()) {
                InitTrueTimeAsyncTask trueTime = new InitTrueTimeAsyncTask(ctx);
                trueTime.execute();
            }
        }
    }

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx
                .getSystemService (Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ni != null && ni.isConnectedOrConnecting();
    }

    //*********TRUE TIME ENDS*************
    public void scan()
    {
        Intent intent = new Intent(this, Scanner.class);
        startActivity(intent);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Find(), "Find");
        adapter.addFragment(new Park(), "Park");
        adapter.addFragment(new Pay(), "Pay");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onPaymentSuccess(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        String state = "paid";
        sharedPrefManager = new SharedPrefManager(mContext);
        sharedPrefManager.saveState(this, state);



        Date trueTime = MainActivity.getTrueTime();
        String checkout_time =Pay._formatDate(trueTime, "yyyy-MM-dd HH:mm:ss", TimeZone.getTimeZone("GMT+05:30"));
        Pay.timeOut.setText(checkout_time);

        String cost = String.valueOf(Pay.billAmount);

        sharedPrefManager.saveTimeOut(mContext, checkout_time);

        mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        String time_out = String.valueOf(MainActivity.getTrueTime());

        DatabaseReference current_parking = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("CurrentParkingInformation");
        current_parking.removeValue();

        DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory").child(checkout_time);

        current_user_db.child("basePriceValid").setValue(sharedPrefManager.getBasePriceValid());
        current_user_db.child("checkOutBeforeTime").setValue(sharedPrefManager.getCheckoutBeforeTime());
        current_user_db.child("parkingName").setValue(sharedPrefManager.getCoordinates());
        current_user_db.child("cost").setValue(cost);
        current_user_db.child("time_in").setValue(sharedPrefManager.getTime_in());
        current_user_db.child("time_out").setValue(checkout_time);
        current_user_db.child("type").setValue(sharedPrefManager.getSelectedVehType());
        current_user_db.child("vehicle_Number").setValue(sharedPrefManager.getSelectedVehNum());
        current_user_db.child("paymentId").setValue(s);
        current_user_db.child("cost_per_hour").setValue(sharedPrefManager.get_cost_per_hour());

        DatabaseReference history_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory");

      //  Pay.StartTimer(sharedPrefManager.getCheckoutBeforeTime());

        String exit_before_time = String.valueOf((60- Pay.ParkingMins)*60000);
        sharedPrefManager.saveExitBefore(mContext,exit_before_time);

       String chkottimems= String.valueOf(MainActivity.getTrueTime().getTime());
       sharedPrefManager.savecheckOutTimeinms(mContext,chkottimems);
       sharedPrefManager.saveLastCost(mContext, String.valueOf(Pay.billAmount));
        sharedPrefManager.saveLastParkingTotalTime(mContext,Pay.LastParkingHour);
       Pay.StartTimer(exit_before_time);


       String str=  "{cost_per_hour="+sharedPrefManager.get_cost_per_hour()+", checkOutBeforeTime="+sharedPrefManager.getCheckoutBeforeTime()+", cost="+cost+", vehicle_Number="+sharedPrefManager.getSelectedVehNum()+", paymentId="+s+", time_in="+sharedPrefManager.getTime_in()+", basePriceValid="+sharedPrefManager.getBasePriceValid()+", type="+bike+", parkingName="+ sharedPrefManager.getCoordinates()+", time_out="+checkout_time+"}";
        SharedPreferences VehicleHistory = getSharedPreferences("SaveHistory", Context.MODE_PRIVATE);
        Set<String> history = VehicleHistory.getStringSet("transaction", null);

        ArrayList<String> historyList = new ArrayList<String>();


        if (history != null) {
            for (String x : history) {
                historyList.add(x);

            }

        }
        historyList.add(str);


        hPreferences = getSharedPreferences("SaveHistory",Context.MODE_PRIVATE);
        Set<String> transaction = new HashSet<String>();
        SharedPreferences.Editor editor= hPreferences.edit();
        transaction.addAll(historyList);
        editor.putStringSet("transaction",transaction);
        editor.commit();
        editor.apply();


    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(this, "PAYMENT FAILED",Toast.LENGTH_SHORT).show();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed(); commented this line in order to disable back press
        //Write your code here
        //   Toast.makeText(getApplicationContext(), "Back press disabled!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.vehicles) {
            // ShowPopup();

            //Intent intent = new Intent(this, VehiclesListActivity.class);
            Intent intent = new Intent(this, VehicleListSwipeView.class);
            startActivity(intent);

        } else if (id == R.id.history) {
            Intent intent = new Intent(this, HistoryListActivity.class);
            startActivity(intent);

        } else if (id == R.id.logout) {
            //Logout
            mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();


            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // ...
                            Toast.makeText(getApplicationContext(),"Logged Out",Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(getApplicationContext(),CustomerLogin.class);
                            startActivity(i);
                        }
                    });




            sharedPrefManager = new SharedPrefManager(mContext);
            sharedPrefManager.clearData(mContext, "EMAIL");
            sharedPrefManager.clearData(mContext, "NAME");
          //  sharedPrefManager.clearData(mContext, "PHOTO");
            sharedPrefManager.clearData(mContext, "EMAIL");

            SharedPreferences VehicleHistory = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = VehicleHistory.edit();
            editor.clear();
            editor.commit();

            SharedPreferences history = getSharedPreferences("SaveHistory", Context.MODE_PRIVATE);
            SharedPreferences.Editor ed = history.edit();
            ed.clear();
            ed.commit();



        } else if (id == R.id.share) {
//            Intent intent = new Intent(this, Share.class);
//            startActivity(intent);


            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
            String shareBodyText = "Hey Check Out the new App i am using for searching parking location near my area on\n" +
                    " Android: https://play.google.com/store/apps/details?id=mobi.parkin.android.parkin\n" +
                    "\n" +
                    "IOS :  https://itunes.apple.com/in/app/parkin/id1446182524?mt=8";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Subject here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
        startActivity(Intent.createChooser(sharingIntent, "Sharing Options"));

        } else if (id == R.id.support) {

            Intent intent = new Intent(this, Support.class);
            startActivity(intent);

        }
        else if (id ==R.id.about_us)
        {
            Uri uri = Uri.parse("http://www.parkin.mobi");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
        else if (id ==R.id.digilocker)
        {
//            Uri uri = Uri.parse("https://digilocker.gov.in/index.php#no-back");
//            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//            startActivity(intent);

//            Intent intent = new Intent(this, Digilocker.class);
//            startActivity(intent);

            Intent intent = new Intent(this, Digilocker.class);
            startActivity(intent);


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void popupSelectVehicle()
    {


        SharedPreferences VehicleHistory = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
        Set<String> Car = VehicleHistory.getStringSet("car",null);
        Set<String> Bike = VehicleHistory.getStringSet("bike",null);
        ArrayList<String> carList = new ArrayList<String>();
        ArrayList<String> bikeList = new ArrayList<String>();

        if (Car!=null) {
            for (String x : Car) {
                carList.add(x);
            }
        }

        if (Bike!=null) {
            for (String y : Bike) {
                bikeList.add(y);
            }
        }
        int x=carList.size()+bikeList.size();
        int j=0, z=0;
        final Vehicle[] vehicles_data = new Vehicle[x];
        for (int i=0; i<x;i++) {
            if (carList.size()!=0 && i<carList.size())
                vehicles_data[i] = new Vehicle(R.mipmap.parkin_car, carList.get(z++));
            else
                vehicles_data[i] = new Vehicle(R.mipmap.parkin_bike, bikeList.get(j++));
        }




        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        final AlertDialog alert = alertDialog.create();

        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.main_listview, null);
//        alertDialog.setView(convertView);
//        alertDialog.setTitle("Select Vehicle");

        alert.setView(convertView);
        alert.setTitle("Select Vehicle");
        ListView lv = (ListView) convertView.findViewById(R.id.listView1);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,names);
        VehicleAdapter adapter = new VehicleAdapter(this,
                R.layout.listview_item_row, vehicles_data);
        lv.setAdapter(adapter);

        alert.show();




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {



            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                alert.cancel();
                Vehicle v = vehicles_data[i];
                String b = v.title;
                int n = v.icon;

                vehNumSelected = b;
                mAuth = FirebaseAuth.getInstance();
                String user_id= mAuth.getCurrentUser().getUid();
                DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);

                String current_state = sharedPrefManager.getState();

if (current_state==null)
    current_state = "normal";

                if (n==2131492905 && !current_state.equalsIgnoreCase("scanned"))
                {
                    vehSelectedType = "bike";
                    current_user_db.child("CurrentParkingInformation").child("number").setValue(vehNumSelected);
                    current_user_db.child("CurrentParkingInformation").child("type").setValue(vehSelectedType);

                    sharedPrefManager.saveSelectedVehNum(mContext, vehNumSelected);
                    sharedPrefManager.saveSelectedVehType(mContext, vehSelectedType);

                }
                else if (n==2131492906 && !current_state.equalsIgnoreCase("scanned"))
                {
                    vehSelectedType = "car";
                    current_user_db.child("CurrentParkingInformation").child("number").setValue(vehNumSelected);
                    current_user_db.child("CurrentParkingInformation").child("type").setValue(vehSelectedType);

                    sharedPrefManager.saveSelectedVehNum(mContext, vehNumSelected);
                    sharedPrefManager.saveSelectedVehType(mContext, vehSelectedType);
                }
//dialog.dismiss();
                scan();


            }
        });



    }

    private boolean isFirstTimeStartApp() {
        SharedPreferences ref = getApplicationContext().getSharedPreferences("IntroSliderApp", Context.MODE_PRIVATE);
        return ref.getBoolean("FirstTimeStartFlag", true);
    }

    private void setFirstTimeStartStatus(boolean stt) {
        SharedPreferences ref = getApplicationContext().getSharedPreferences("IntroSliderApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = ref.edit();
        editor.putBoolean("FirstTimeStartFlag", stt);
        editor.commit();
    }

    private void startMainActivity(){
        setFirstTimeStartStatus(false);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) mContext)
                                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                                    .addApi(LocationServices.API)
                                    .build();

                            mGoogleApiClient.connect();
                        }
                       // mMap.setMyLocationEnabled(true);
                        loadFragment(new Find());
                    }
                } else// Permission is denied
                {
                    Toast.makeText(mContext, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                return;
        }
    }


    private  void firebaseVehicle()
    {


        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        vehicleDBCar = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles").child("Car");
        vehicleDBBike = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID).child("Vehicles").child("Bike");


        vehicleDBBike.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long values = dataSnapshot.getChildrenCount();
                Log.d(TAG, "no of children: " + values);

                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>() {
                };


                List<String> taskDesLists = dataSnapshot.getValue(genericTypeIndicator);


//            for(int i=0;i<taskDesLists.size();i++){
//                //Toast.makeText(MainActivity.this,"TaskTitle = "+taskDesList.get(i).getTaskTitle(),Toast.LENGTH_LONG).show();
//                Log.d("list size", String.valueOf(taskDesLists.size()));
//            }
                hPreferences = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
                // Set<String> car = hPreferences.getStringSet("car",null);
                Set<String> bike = new HashSet<String>();
                SharedPreferences.Editor editor = hPreferences.edit();
                //  Set<String> car = new HashSet<String>();

                if (taskDesLists != null) {
                    bike.addAll(taskDesLists);
                }
                editor.putStringSet("bike", bike);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        vehicleDBCar.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long value = dataSnapshot.getChildrenCount();
                Log.d(TAG, "no of children: " + value);

                GenericTypeIndicator<List<String>> genericTypeIndicator = new GenericTypeIndicator<List<String>>() {
                };

                List<String> taskDesList = dataSnapshot.getValue(genericTypeIndicator);

//            for(int i=0;i<taskDesList.size();i++){
//                //Toast.makeText(MainActivity.this,"TaskTitle = "+taskDesList.get(i).getTaskTitle(),Toast.LENGTH_LONG).show();
//                Log.d("list size", String.valueOf(taskDesList.size()));
//            }
                hPreferences = getSharedPreferences("SaveVehicle", Context.MODE_PRIVATE);
                // Set<String> car = hPreferences.getStringSet("car",null);
                Set<String> car = new HashSet<String>();
                SharedPreferences.Editor editor = hPreferences.edit();
                //  Set<String> car = new HashSet<String>();

                if (taskDesList != null) {
                    car.addAll(taskDesList);
                }

                editor.putStringSet("car", car);
                editor.commit();
                editor.apply();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

}
