package mobi.parkin.android.parkindelivery;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Set;

public class ItemsListActivity extends AppCompatActivity {

    SharedPreferences hPreferences;
    private String userID;

    public String itemsName, itemsPrice, itemsWeight;

    private FirebaseAuth mAuth;
    public SharedPrefManager sharedPrefManager;

    private DatabaseReference itemsDB;
    ArrayList<String> ItemsList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_list);

        RadioGroup radioGroup;
      //  radioGroup = (RadioGroup) myDialog.findViewById(R.id.toggle);

        final SwipeMenuListView listViewitems = (SwipeMenuListView) findViewById(R.id.itemsswipelistview);
        View header = (View) getLayoutInflater().inflate(R.layout.items_header_row, null);
        listViewitems.addHeaderView(header);

        SharedPreferences storeItems = getSharedPreferences("SaveItems", Context.MODE_PRIVATE);
        Set<String> items = storeItems.getStringSet("itemsBox", null);

        ArrayList<String> itemsList = new ArrayList<String>();
        final ArrayList<String> itemsdataList = new ArrayList<String>();

        if (items != null) {
            for (String x : items) {
                itemsList.add(x);


            }
        }

        int x = itemsList.size();

        final Items[] items_data = new Items[x];
        for (int i = 0; i < x; i++) {

            final String rawString = itemsList.get(i);
            String[] arrOfStrs = rawString.split(",");
            itemsName = arrOfStrs[0];
            itemsPrice = arrOfStrs[1];
            itemsWeight = arrOfStrs[2];

            if (itemsList.size() != 0 && i < itemsList.size()) {

                  items_data[i] = new Items(R.mipmap.parkin_bike, itemsName, itemsPrice, itemsWeight);

            }

        }
        ItemsAdapter adapter = new ItemsAdapter(this,
                R.layout.itemslistview_item_row, items_data);



        listViewitems.setAdapter(adapter);
        adapter.notifyDataSetChanged();





    }
}
