package mobi.parkin.android.parkindelivery;

public class History {


    public int icon;
    public String title;
    public String cost;
    public String timein;
    public String timeout;
    public String vehicle;

    public History(){
        super();
    }


    public History(int icon, String title, String cost, String timein ,String timeout, String vehicle) {
        super();
        this.icon = icon;
        this.title = title;
        this.cost = cost;
        this.timein = timein;
        this.timeout = timeout;
        this.vehicle = vehicle;
    }
}
