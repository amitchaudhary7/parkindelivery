package mobi.parkin.android.parkindelivery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class ParkedLocation extends AppCompatActivity {

    private TextView VehNumTitle, VehNum,ParkingLoc,Coordinates,CheckedTime,time;
    private ImageView VehTypeIcon;
    SharedPrefManager sharedPrefManager;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parked_location);

        VehTypeIcon = (ImageView)findViewById(R.id.VehTypeicon);
        VehNumTitle = (TextView)findViewById(R.id.ParkedVehicleNum);
        VehNum = (TextView)findViewById(R.id.VNum);
        ParkingLoc = (TextView)findViewById(R.id.ParkingLoc);
        Coordinates = (TextView)findViewById(R.id.Coordinates);
        CheckedTime = (TextView)findViewById(R.id.CheckedTime);
        time = (TextView)findViewById(R.id.time);

        sharedPrefManager = new SharedPrefManager(this);

        mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);


        Date trueTime = MainActivity.getTrueTime();

        String x =_formatDate(trueTime, "yyyy-MM-dd HH:mm:ss", TimeZone.getTimeZone("GMT+05:30"));

        time.setText(x);
        current_user_db.child("CurrentParkingInformation").child("time_In").setValue(x);

        String i = sharedPrefManager.getSelectedVehType();
        if (sharedPrefManager.getSelectedVehType().equalsIgnoreCase("car"))
        {
            VehTypeIcon.setImageResource(R.mipmap.parkin_car);
            current_user_db.child("CurrentParkingInformation").child("type").setValue("car");
        }
        else
        {
            VehTypeIcon.setImageResource(R.mipmap.parkin_bike);
            current_user_db.child("CurrentParkingInformation").child("type").setValue("bike");
        }

        VehNum.setText(MainActivity.vehNumSelected);
        current_user_db.child("CurrentParkingInformation").child("number").setValue(MainActivity.vehNumSelected);


        Coordinates.setText(sharedPrefManager.getCoordinates());






    }





    private String _formatDate(Date date, String pattern, TimeZone timeZone) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        format.setTimeZone(timeZone);
        return format.format(date);
    }
}

