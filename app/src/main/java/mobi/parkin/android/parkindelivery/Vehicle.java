package mobi.parkin.android.parkindelivery;

public class Vehicle {

    public int icon;
    public String title;
    public Vehicle(){
        super();
    }


    public Vehicle(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}
