package mobi.parkin.android.parkindelivery;

import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import cn.iwgang.countdownview.CountdownView;

import static java.lang.Math.ceil;

//LATEST

public class Pay extends Fragment implements PaymentResultListener,LifecycleObserver {

    SharedPrefManager sharedPrefManager;

    public  static Context mContext = null;

    public   static ImageView imageView;
    private TextView SelectVehicle;
    private  static  TextView totaltime,parkinghrs,TotalCost,cost;
    private static HorizontalScrollView horizontalScrollView;
    private Button payment;
    private Button confirmpayment;

    public   ImageView VehicleTypeSelected;
    public  TextView VehSelected;
    private  String a,b ;

    private String carNumDropdown;
    public Spinner dropdown;

    public long checkOutSystemtime ;
    public long checkinSystemtime ;
    public static long difference ;
    public static double ParkingHours ;
    public static int ParkingHoursActual ;
    public static int ParkingMins ;
    public static double ParkingSecs ;
    public long checkoutTime;
    public long y;

    public  static  String LastParkingHour;

    private RadioGroup radioGroup;

    private Task<Void> mCustomerDatabase;
    private  String userID;

    public Long duration;

    public static boolean car= true;
    public static boolean bike;


    private  TextView locationtxt;
    public static TextView locationckdin;
    private  TextView Vehiclenumtxt;
    private static TextView numberplates;
    private  TextView VehicleTypetxt;
    private static TextView vtype;
    private  TextView BaseCharge;
    private static TextView bcharge;
    private  TextView costphrtxt;
    private static TextView cph;
    private  TextView time_intxt;
    private static TextView time;
    private  TextView basecharevalidtxt;
    private static TextView validity;
    private  TextView chkouttime;
    public static TextView timeOut;

    public static CountdownView mCvCountdownView;

    public  long times;
    public  long charge;
    public static long billAmount;
    public  String costs;
    int payamount;

    private FirebaseAuth mAuth;

    public Pay() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pay, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);


        mContext = getActivity();

        payment = (Button) getView().findViewById(R.id.payment);
        confirmpayment = (Button)getView().findViewById(R.id.confirmpayment);

       // radioGroup = (RadioGroup) getView().findViewById(R.id.toggle);

        SelectVehicle = (TextView) getView().findViewById(R.id.SelectVehicle);

        locationtxt = (TextView) getView().findViewById(R.id.location);
        locationckdin = (TextView) getView().findViewById(R.id.checkedlocation);
        Vehiclenumtxt = (TextView) getView().findViewById(R.id.VehicleType);
        VehicleTypetxt = (TextView) getView().findViewById(R.id.VehicleNum);
        numberplates= (TextView) getView().findViewById(R.id.numberPlate);
        vtype = (TextView) getView().findViewById(R.id.car_bike);
        BaseCharge = (TextView) getView().findViewById(R.id.baseCharge);
        bcharge = (TextView) getView().findViewById(R.id.charge);
        costphrtxt = (TextView) getView().findViewById(R.id.cph);
        cph = (TextView) getView().findViewById(R.id.costperhr);
        time_intxt = (TextView) getView().findViewById(R.id.chkintime);
        time = (TextView) getView().findViewById(R.id.time);
        basecharevalidtxt = (TextView) getView().findViewById(R.id.basechargeValidity);
        validity = (TextView) getView().findViewById(R.id.validity);
        timeOut = (TextView) getView().findViewById(R.id.timeOut);

        totaltime = (TextView) getView().findViewById(R.id.totaltime);
        parkinghrs = (TextView) getView().findViewById(R.id.parkinghrs);
        TotalCost = (TextView) getView().findViewById(R.id.TotalCost);
        cost = (TextView) getView().findViewById(R.id.cost);

        horizontalScrollView=(HorizontalScrollView) getView().findViewById(R.id.hsv);



        VehSelected = (TextView) getView().findViewById(R.id.VehSelected);




        mCvCountdownView = (CountdownView)getView().findViewById(R.id.cv_countdownViewTest1);

        sharedPrefManager = new SharedPrefManager(getContext());
//        if (sharedPrefManager.getCheckoutBeforeTime()!=null)
//        { y = Long.parseLong(sharedPrefManager.getCheckoutBeforeTime())*1000;}

        if (sharedPrefManager.getExitBefore()!=null)
        { y = Long.parseLong(sharedPrefManager.getExitBefore())*60*1000;}



        if (sharedPrefManager.getcheckOutTimeinms() != null) {
            if (Long.parseLong(sharedPrefManager.getcheckOutTimeinms()) != 0) {
                long x = MainActivity.getTrueTime().getTime() - Long.parseLong(sharedPrefManager.getcheckOutTimeinms());

                // mCvCountdownView.setVisibility(View.VISIBLE);

                //  mCvCountdownView.start(y-x);

            }
        }

        //   imageView = (ImageView)getView().findViewById(R.id.imageView3);


        sharedPrefManager = new SharedPrefManager(getContext());

        String x= sharedPrefManager.getState();


        if (x!=null) {

            if (x.equalsIgnoreCase("paid")) {

                mCvCountdownView.setVisibility(View.VISIBLE);
                totaltime.setVisibility(View.VISIBLE);
                parkinghrs.setVisibility(View.VISIBLE);
                TotalCost.setVisibility(View.VISIBLE);
                cost.setVisibility(View.VISIBLE);
                horizontalScrollView.setVisibility(View.VISIBLE);


                long s = Long.parseLong(sharedPrefManager.getExitBefore());

                parkinghrs.setText(sharedPrefManager.getLastParkingTotalTime());

                String costs= sharedPrefManager.getLastCost();
                cost.setText("₹ "+ costs);


                String t = sharedPrefManager.getcheckOutTimeinms();

                if (t!= null) {
                    long z = MainActivity.getTrueTime().getTime()- Long.parseLong(t);
                    mCvCountdownView.start(s-z);

                }

            }

        }


        if (x!=null)

        {
            if (x.equalsIgnoreCase("scanned") || x.equalsIgnoreCase("paid")) {
                locationckdin.setText(sharedPrefManager.getCoordinates());


                numberplates.setText(sharedPrefManager.getSelectedVehNum());


                vtype.setText(sharedPrefManager.getSelectedVehType());

                if (sharedPrefManager.getSelectedVehType() == "car") {

                    String s = sharedPrefManager.getCarPrices();


                    String[] ss = s.split(",");

                    bcharge.setText(ss[0]);
                    cph.setText(ss[1]);

                } else {
                    String s = sharedPrefManager.getBikePrices();

                    String[] ss = s.split(",");

                    bcharge.setText(ss[0]);
                    cph.setText(ss[1]);
                }


                time.setText(sharedPrefManager.getTime_in());

                long gBpV = Long.parseLong(sharedPrefManager.getBasePriceValid()) / 3600;

                validity.setText(String.valueOf(gBpV) + " Hr");


                if (x.equalsIgnoreCase("paid")) {
                    timeOut.setText(sharedPrefManager.getTime_out());
                }
//            String y =_formatDate(trueTime, "yyyy-MM-dd HH:mm:ss", TimeZone.getTimeZone("GMT+05:30"));

            }


        }


        mCvCountdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {
                //    imageView.setImageResource(R.mipmap.greenpark);
                sharedPrefManager.saveState(getActivity(),"normal");
            }
        });




        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String currentstate = sharedPrefManager.getState();

                if (currentstate != null) {
                    if (currentstate.equalsIgnoreCase("scanned")) {
                        checkoutTime = MainActivity.getTrueTime().getTime();
                        Log.d("CheckOut", String.valueOf(checkoutTime));

                        sharedPrefManager.saveCheckoutTime(getActivity(), String.valueOf(checkoutTime));


                        if (sharedPrefManager.getcheckOutTimeinms() != null) {
                            long checkintime_ms = Long.parseLong(sharedPrefManager.getcheckInTimeinms());


                            difference = checkoutTime - checkintime_ms;
                            ParkingHours = difference / 1000; //sec
                            ParkingHoursActual = (int) (ParkingHours / 3600);
                            ParkingMins = (int) ((ParkingHours % 3600) / 60);
                            ParkingSecs = ParkingHours % 60;
                            ParkingHours = ceil(ParkingHours / 3600); //hrs


                            Log.d("difference", String.valueOf(ParkingHoursActual));
                            Log.d("ParkingHours", String.valueOf(ParkingMins));
                            Log.d("difference", String.valueOf(ParkingSecs));
                            Log.d("difference", String.valueOf(difference));
                            Log.d("ParkingHours", String.valueOf(ParkingHours));

                        }
//                if (String.valueOf(ParkingHoursActual)== "0" && String.valueOf(ParkingMins)== "0")
//                {
//                    ParkingMins=1;
//                }

                        LastParkingHour = String.valueOf(ParkingHoursActual) + " Hrs " + String.valueOf(ParkingMins) + " Mins";

                        parkinghrs.setText(LastParkingHour);


                        long x = MainActivity.getTrueTime().getTime() - checkoutTime;
                        long y = Long.parseLong(sharedPrefManager.getCheckoutBeforeTime()) * 1000;
//
//                mCvCountdownView.setVisibility(View.VISIBLE);
//                mCvCountdownView.start(y-x);


                        totaltime.setVisibility(View.VISIBLE);
                        parkinghrs.setVisibility(View.VISIBLE);
                        TotalCost.setVisibility(View.VISIBLE);
                        cost.setVisibility(View.VISIBLE);
                        horizontalScrollView.setVisibility(View.VISIBLE);
                        pay();
                    } else if (currentstate.equalsIgnoreCase("paid")) {
                        //  Snackbar snackbar= Snackbar.make(coordinatorlayout,)

                        //     Snackbar.make(getActivity().findViewById(android.R.id.content),
                        //           "You paid the bill", Snackbar.LENGTH_LONG).show();

                        Snackbar.make(getActivity().findViewById(android.R.id.content), "You paid the bill", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();


                    } else if (currentstate.equalsIgnoreCase("normal")) {

                        Snackbar.make(getActivity().findViewById(android.R.id.content),
                                "Please check In", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }



    public static long getduration()
    {
        return (long) ParkingHours;
    }



    public void pay()
    {


        charge = BillAmount();
        costs = String.valueOf(charge);


        cost.setText("₹ "+ costs);




        //   String f= String.valueOf(ParkingHoursActual)+ " Hrs " + String.valueOf(ParkingMins) + " Mins";
        //   parkinghrs.setText(f);
        payment.setVisibility(View.INVISIBLE);
        confirmpayment.setVisibility(View.VISIBLE);

        confirmpayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

       //         Snackbar.make(getActivity().findViewById(android.R.id.content), "Coming Soon !", Snackbar.LENGTH_LONG)
       //                .setAction("Action", null).show();

           // BETA RELEASE HOLDS THE PAYMENT GATEWAY
                payamount=Integer.parseInt(String.valueOf(costs));


                Checkout checkout = new Checkout();

                // checkout.setImage(R.mipmap.card);//logo
                final Activity activity= getActivity();

                try{

                    JSONObject options = new JSONObject();
                    options.put("description", sharedPrefManager.getCoordinates());

                    options.put("currency","INR");

                    options.put("amount",payamount*100);

                    options.put("timeout" ,120);



                    checkout.open(activity,options);



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public  static void StartTimer( String gcot)
    {

//    long cot = MainActivity.getTrueTime().getTime();
//    long x = MainActivity.getTrueTime().getTime()- cot;
//    long y = Long.parseLong(gcot)*1000;
        long t = Long.parseLong(gcot);
        mCvCountdownView.setVisibility(View.VISIBLE);

        mCvCountdownView.start(t);
        // mCvCountdownView.start(y-x);
    }

    public  long BillAmount()
    {
        sharedPrefManager = new SharedPrefManager(mContext);

        checkoutTime = MainActivity.getTrueTime().getTime();
        if (sharedPrefManager.getcheckOutTimeinms()!=null) {
            long checkintime_ms = Long.parseLong(sharedPrefManager.getcheckInTimeinms());

            difference = checkoutTime - checkintime_ms;
            ParkingHours = difference / 1000; //sec
            ParkingHours = ceil(ParkingHours / 3600); //hrs

        }


        String x= sharedPrefManager.getSelectedVehType();
        if (x!=null) {
            if (x.equalsIgnoreCase("car")) {
                // billAmount = Scanner.CarCharge()* Pay.getduration();

                String str = sharedPrefManager.getCarPrices();
                String[] arrStr = str.split(",");
                long CarBase = Long.parseLong(arrStr[0]);
                long CarAdOn = Long.parseLong(arrStr[1]);
                long BaseValidity = Long.parseLong(sharedPrefManager.getBasePriceValid()) / 3600;

                String cost_per_hour = String.valueOf(CarBase)+","+String.valueOf(CarAdOn);
                sharedPrefManager.save_cost_per_hour(getActivity(),cost_per_hour);

                if (ParkingHours > BaseValidity) {
                    billAmount = (long) (CarBase + CarAdOn * (ParkingHours - BaseValidity));
                } else {
                    billAmount = CarBase;
                }
            } else if (x.equalsIgnoreCase("bike")) {
                String str2 = sharedPrefManager.getBikePrices();
                String[] arrStr2 = str2.split(",");
                long BikeBase = Long.parseLong(arrStr2[0]);
                long BikeAdOn = Long.parseLong(arrStr2[1]);
                long BaseValidity = Long.parseLong(sharedPrefManager.getBasePriceValid()) / 3600;

                String cost_per_hour = String.valueOf(BikeBase)+","+String.valueOf(BikeAdOn);
                sharedPrefManager.save_cost_per_hour(getActivity(),cost_per_hour);

                //billAmount = BikeBase+ BikeAdOn* Pay.getduration();
                if (ParkingHours > BaseValidity) {
                    billAmount = (long) (BikeBase + BikeAdOn * (ParkingHours - BaseValidity));
                } else {
                    billAmount = BikeBase;
                }
            }

        }

//        Pay.redpark();

        return billAmount;
    }


    @Override
    public void onStart() {
        super.onStart();
        //mAuth.addAuthStateListener(mAuthListener);
    }

    public static boolean CarSelected()
    {

        return car;
    }

    public static boolean BikeSelected()
    {

        return bike;
    }


    public static void orangepark(){

    }

    public static void updateTextViews(String a,String b, String c,String d, String e, String f){


        locationckdin.setText(a);

        timeOut.setText("-");


        numberplates.setText(b);

        mCvCountdownView.setVisibility(View.INVISIBLE);
        totaltime.setVisibility(View.INVISIBLE);
        parkinghrs.setVisibility(View.INVISIBLE);
        TotalCost.setVisibility(View.INVISIBLE);
        cost.setVisibility(View.INVISIBLE);
        horizontalScrollView.setVisibility(View.INVISIBLE);




        vtype.setText(c);

        if (c=="car") {

            String s = e;


            String[] ss = s.split(",");

            bcharge.setText(ss[0]);
            cph.setText(ss[1]);

        }
        else
        {
            String s = d;

            String[] ss = s.split(",");

            bcharge.setText(ss[0]);
            cph.setText(ss[1]);
        }


        Date trueTime = MainActivity.getTrueTime();
        String y =_formatDate(trueTime, "yyyy-MM-dd HH:mm:ss", TimeZone.getTimeZone("GMT+05:30"));
        time.setText(y);

        validity.setText(f);




    }

    public static  String _formatDate(Date date, String pattern, TimeZone timeZone) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        format.setTimeZone(timeZone);
        return format.format(date);
    }

    public static void redpark()
    {
//        imageView.setImageResource(R.mipmap.redpark);
    }


    @Override
    public void onPaymentSuccess(String s) { // Implemented in Main Activity
        Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();


        mAuth = FirebaseAuth.getInstance();
        String user_id= mAuth.getCurrentUser().getUid();
        String time_out = String.valueOf(MainActivity.getTrueTime());
        DatabaseReference current_parking = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("CurrentParkingInformation");
        current_parking.removeValue();

        DatabaseReference current_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory").child(time_out);

        current_user_db.child("basePriceValid").setValue(sharedPrefManager.getBasePriceValid());
        current_user_db.child("checkOutBeforeTime").setValue(sharedPrefManager.getCheckoutBeforeTime());
        current_user_db.child("parkingName").setValue(sharedPrefManager.getCoordinates());
        current_user_db.child("cost").setValue(billAmount);
        current_user_db.child("time_in").setValue(sharedPrefManager.getTime_in());
        current_user_db.child("time_out").setValue(time_out);
        current_user_db.child("type").setValue(sharedPrefManager.getSelectedVehType());
        current_user_db.child("vehicle_Number").setValue(sharedPrefManager.getSelectedVehNum());

        DatabaseReference history_user_db= FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id).child("UserParkingHistory");

    }

    @Override
    public void onPaymentError(int i, String s) { // Implemented in Main Activity
        Toast.makeText(getActivity(), "PAYMENT FAILED",Toast.LENGTH_SHORT).show();
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public  void onMoveToForeground()
    {

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public  void onMoveToBackground()
    {

    }
}