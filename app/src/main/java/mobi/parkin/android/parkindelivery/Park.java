package mobi.parkin.android.parkindelivery;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class Park extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;

    private GoogleApiClient mGoogleApiClient;
    public static final int REQUEST_LOCATION_CODE = 99;
    private Context mContext;

    private Marker parkedMarker;
    private  LatLng parkedLocation ;
    private LatLng parkPoint;
    private LatLng navLocation;
    Location mLastLocation;
    private Button FindCar;
    private Button ParkHere;
    private TextView qstn;
    Double end_latitude, end_longitude;
    double latitude, longitude;
    SharedPrefManager sharedPrefManager;
     public static boolean bool;

    public Park() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.mContext = context;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            getActivity().finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_find, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
//        latitude= 13.04089;
//        longitude = 77.618386;
//        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            latitude= mLastLocation.getLatitude();
//            longitude = mLastLocation.getLongitude();
//            return;
//        }




        ParkHere = (Button) getView().findViewById(R.id.ParkHere);
        ParkHere.setVisibility(View.VISIBLE);
        FindCar = (Button) getView().findViewById(R.id.FindCar);
        FindCar.setVisibility(View.VISIBLE);

        qstn= (TextView)getView().findViewById(R.id.textView2) ;
        qstn.setVisibility(View.VISIBLE);


        ParkHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    case R.id.ParkHere:


                        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                       Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                        parkedLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        mMap.clear();
                        parkedMarker = mMap.addMarker(new MarkerOptions().position(parkedLocation).title("Parked Here").icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_car)));
                        ;
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parkedLocation, 14));
                        Toast.makeText(getActivity(), "Vehicle Parked", Toast.LENGTH_LONG).show();
                        //  end_latitude=mLastLocation.getLatitude();
                        end_latitude = parkedLocation.latitude;
                        end_longitude = parkedLocation.longitude;


                        sharedPrefManager = new SharedPrefManager(getActivity());
                        sharedPrefManager.saveParkLat(getActivity(), String.valueOf(parkedLocation.latitude));
                        sharedPrefManager.saveParkLng(getActivity(), String.valueOf(parkedLocation.longitude));

                        bool=true;

                        ParkHere.setBackgroundColor(R.color.orange);
                        break;


                }


            }


        });

        FindCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Object dataTransfer[] = new Object[2];

         if (end_latitude!=null && end_longitude != null) {
             dataTransfer = new Object[3];
             String url = getDirectionsUrl();
             GetDirectionsData getDirectionsData = new GetDirectionsData();
             dataTransfer[0] = mMap;
             dataTransfer[1] = url;
             dataTransfer[2] = new LatLng(end_latitude, end_longitude);
             getDirectionsData.execute(dataTransfer);

             Toast.makeText(getActivity(), "Getting back", Toast.LENGTH_LONG).show();
             FindCar.setBackgroundColor(R.color.orange);

         }
         else
         {
             Toast.makeText(getActivity(), "Please get parked", Toast.LENGTH_LONG).show();
         }

            }
        });
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_CODE);
            }
            return false;
        } else
            return true;

    }

    private String getDirectionsUrl() {
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
          Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
          navLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
           latitude = navLocation.latitude;
           longitude= navLocation.longitude;
        StringBuilder googleDirectionsUrl = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        googleDirectionsUrl.append("origin="+latitude+","+longitude);
        googleDirectionsUrl.append("&destination="+end_latitude+","+end_longitude);
        googleDirectionsUrl.append("&key="+"AIzaSyBXltARx-ZGWq71njmQ_dUJQGrdq6gC0m8");

        Log.d("getUrl", "getDirectionsUrl Called");
        Log.d("getUrl data", String.valueOf(googleDirectionsUrl));
        return googleDirectionsUrl.toString();

    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(mContext);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                                    .addConnectionCallbacks(this)
                                    .addOnConnectionFailedListener(this)
                                    .addApi(LocationServices.API)
                                    .build();

                            mGoogleApiClient.connect();
                        }
                        mMap.setMyLocationEnabled(true);

                    }
                } else// Permission is denied
                {
                    Toast.makeText(mContext, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                return;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        // Add a marker in Sydney and move the camera
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation==null)
        {
            Toast.makeText(getActivity(), "Location is off", Toast.LENGTH_LONG).show();
        }
        else {
            LatLng here = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            //  mMap.addMarker(new MarkerOptions().position(here).title("YOU ARE HERE").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(here, 14));
        }
                sharedPrefManager = new SharedPrefManager(getActivity());
if (bool == true) {
    end_latitude = Double.valueOf(sharedPrefManager.getParkLat());
    end_longitude = Double.valueOf(sharedPrefManager.getParkLng());

    parkPoint = new LatLng(end_latitude, end_longitude);
    parkedMarker = mMap.addMarker(new MarkerOptions().position(parkPoint).title("Parked Here").icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_car)));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(parkPoint, 14));
}

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Get location
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mGoogleApiClient.connect();
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.setTrafficEnabled(true);
        }
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onStart() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        super.onStart();
    }

    public void onStop() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }
}